﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections;
using System.Collections.Generic;
using System;

namespace Cube
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D testsprite;
        Texture2D hollow;
        static public Texture2D dot;
        Matrix m;
        List<Vector2> points;
        spinning_cube sCube;
        List<Texture2D> Diefaces;
        float animate = 0;
        MouseState ms;
        SpriteFont f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }
        protected override void Initialize()
        {
            graphics.PreferredBackBufferHeight = 800;
            graphics.PreferredBackBufferWidth = 800;
            graphics.ApplyChanges(); 

            sCube = new spinning_cube();
            points = new List<Vector2>();
            Diefaces = new List<Texture2D>();
            base.Initialize();

            points.AddRange(new Vector2[] { new Vector2(100), new Vector2(190,90),new Vector2(90,180) });
            IsMouseVisible = true;

            m = Matrix.CreateRotationZ((float)Math.Atan2(points[1].Y - points[0].Y, points[1].X - points[0].X)) * Matrix.CreateScale((points[0]-points[1]).Length()/testsprite.Width,(points[0]-points[2]).Length()/testsprite.Height,1) * Matrix.CreateTranslation(new Vector3(points[0], 0));

        }
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            testsprite = Content.Load<Texture2D>("redgreenrect");
            dot = Content.Load<Texture2D>("orangedot");
            hollow = Content.Load<Texture2D>("hollow cube");
            f = Content.Load<SpriteFont>("font");

            Diefaces.Add(Content.Load<Texture2D>("1"));
            Diefaces.Add(Content.Load<Texture2D>("2"));
            Diefaces.Add(Content.Load<Texture2D>("3"));
            Diefaces.Add(Content.Load<Texture2D>("4"));
            Diefaces.Add(Content.Load<Texture2D>("5"));
            Diefaces.Add(Content.Load<Texture2D>("6"));
        }

        protected override void UnloadContent()
        {

        }

        public void DrawTex(Texture2D texture,SpriteBatch s,Vector3[] points,Vector2 texloc)
        {
            if (points.Length != 3)
            {
                return;
            }
            m = Matrix.Identity;
            Vector2 scale = new Vector2(new Vector2(points[0].X - points[1].X, points[0].Y - points[1].Y).Length() / texture.Width*4, -new Vector2(points[0].X - points[2].X, points[0].Y - points[2].Y).Length() * (float)Math.Cos(Math.Atan2(points[0].X - points[2].X, points[0].Y - points[2].Y) + Math.Atan2(points[1].Y - points[0].Y, points[1].X - points[0].X)) / texture.Height*3);
            m *= Matrix.CreateScale(new Vector3( scale, 1));
            m.M21 += (scale.Y) * (float)(Math.Tan(Math.Atan2((points[2].X - points[0].X) , (points[2].Y - points[0].Y)) + Math.Atan2((points[0].Y - points[1].Y), (points[0].X - points[1].X))));
            m *= Matrix.CreateRotationZ((float)Math.Atan2(points[1].Y - points[0].Y, points[1].X - points[0].X));
            m *= Matrix.CreateTranslation(new Vector3(points[0].X + sCube.offsetx, points[0].Y + sCube.offsety, 0));
            s.Begin(SpriteSortMode.FrontToBack,null,null,null,null,null, m);
            s.Draw(texture, Vector2.Zero, new Rectangle((int)texloc.X , (int)texloc.Y, texture.Width / 4, texture.Height / 3),Color.White);
            //s.Draw(texture,Vector2.Zero,new Color(Color.White,0.4f));
            s.End();
            /*
            //m = Matrix.CreateScale(new Vector2(points[0].X - points[1].X, points[0].Y - points[1].Y).Length() / texture.Width, new Vector2(points[0].X - points[2].X, points[0].Y - points[2].Y).Length() / texture.Height, 1) * Matrix.CreateRotationZ((float)Math.Atan2(points[1].Y - points[0].Y, points[1].X - points[0].X));
            m = Matrix.Identity;
            m.M21 = (float)Math.Sin(animate) * 1;
            m *= Matrix.CreateTranslation(new Vector3(points[0].X + sCube.offsetx, points[0].Y + sCube.offsety, 0));
            s.Begin(SpriteSortMode.FrontToBack, null, null, null, null, null, m);
            s.Draw(texture, Vector2.Zero, Color.White);
            s.End();
            */
        }

        protected override void Update(GameTime gameTime)
        {
            ms = Mouse.GetState();
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.LeftAlt))
            {
                points[0] = new Vector2(ms.X, ms.Y);
            }
            if (ks.IsKeyDown(Keys.LeftControl))
            {
                points[1] = new Vector2(ms.X, ms.Y);
            }
            if (ks.IsKeyDown(Keys.LeftShift))
            {
                points[2] = new Vector2(ms.X, ms.Y);
            }
            if (ks.IsKeyDown(Keys.Up))
            {
                sCube.Rotate(new Vector3(0, 0, 0.03f));
            }
            if (ks.IsKeyDown(Keys.Down))
            {
                sCube.Rotate(new Vector3(0, 0, -0.03f));
            }
            if (ks.IsKeyDown(Keys.Left))
            {
                sCube.Rotate(new Vector3(0, 0.03f, 0));
            }
            if (ks.IsKeyDown(Keys.Right))
            {
                sCube.Rotate(new Vector3(0, -0.03f, 0));
            }


            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();
            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin(SpriteSortMode.FrontToBack, null, null, null, null, null, m);
            spriteBatch.End();

            Point[] p = new Point[] { new Point(2, 0), new Point(0,1), new Point(1, 1), new Point(2, 1), new Point(3, 1), new Point(2, 2) };
            Vector3[] s = new Vector3[] { new Vector3(2, 3, 0), new Vector3(3, 2, 7), new Vector3(2, 0, 6), new Vector3(0, 1, 4), new Vector3(1, 3, 5), new Vector3(4, 5, 6) };

            for (int i = 0; i < p.Length; i++)
            {
                DrawTex(hollow, spriteBatch, new Vector3[] { sCube.Cube[(int)s[i].X], sCube.Cube[(int)s[i].Y], sCube.Cube[(int)s[i].Z] }, new Vector2(hollow.Width / 4 * p[i].X, hollow.Height / 3 * p[i].Y));
            }
            
            spriteBatch.Begin();
            /*

            foreach (Vector2 v in points)
            {
                //spriteBatch.Draw(dot,new Vector2(-4 + sCube.offsetx, -4 + sCube.offsety) + v,Color.White);
            }
            */
            //sCube.Draw(spriteBatch);
            
            //sCube.DrawLine(new Vector3(-100, (float)Math.Sin(animate) * 50,0), new Vector3(100, (float)Math.Sin(animate) * 50,0), spriteBatch);
            for (int i = 0; i < sCube.Cube.Length; i++)
            {
                //spriteBatch.DrawString(f, i.ToString(), new Vector2(sCube.Cube[i].X + sCube.offsetx, sCube.Cube[i].Y + sCube.offsety), Color.White);
            }
            
            spriteBatch.End();
            base.Draw(gameTime);
            animate += 0.01f;
        }
    }
}
