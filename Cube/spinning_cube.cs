﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Cube
{
    class spinning_cube
    {

        public int offsetx = 400;
        public int offsety = 400;
        public Vector3[] Cube;
        public spinning_cube()
        {
            int size = 80;
            Cube = new Vector3[] { new Vector3(size, size, size), new Vector3(size, size, -size), new Vector3(size, -size, size), new Vector3(size, -size, -size), new Vector3(-size, size, size), new Vector3(-size, size, -size), new Vector3(-size, -size, size), new Vector3(-size, -size, -size) };
        }

        public void Draw(SpriteBatch s)
        {
            int back = 0;
            for (int i = 1; i < Cube.Length; i++)
            {
                if (Cube[i].Z < Cube[back].Z)
                {
                    back = i;
                }
            }
            if (back != 0 && back != 1)
            {
                DrawLine(Cube[0], Cube[1], s);
            }
            if (back != 1 && back != 3)
            {
                DrawLine(Cube[1], Cube[3], s);
            }
            if (back != 2 && back != 3)
            {
                DrawLine(Cube[2], Cube[3], s);
            }
            if (back != 0 && back != 2)
            {
                DrawLine(Cube[0], Cube[2], s);
            }
            if (back != 4 && back != 5)
            {
                DrawLine(Cube[4], Cube[5], s);
            }
            if (back != 5 && back != 7)
            {
                DrawLine(Cube[5], Cube[7], s);
            }
            if (back != 6 && back != 7)
            {
                DrawLine(Cube[6], Cube[7], s);
            }
            if (back != 6 && back != 4)
            {
                DrawLine(Cube[6], Cube[4], s);
            }
            if (back != 0 && back != 4)
            {
                DrawLine(Cube[0], Cube[4], s);
            }
            if (back != 1 && back != 5)
            {
                DrawLine(Cube[1], Cube[5], s);
            }
            if (back != 2 && back != 6)
            {
                DrawLine(Cube[2], Cube[6], s);
            }
            if (back != 3 && back != 7)
            {
                DrawLine(Cube[3], Cube[7], s);
            }
            
        }

        public void Rotate(Vector3 r)
        {
            for (int i = 0; i < Cube.Length; i++)
            {
                rotate(Cube[i], r, out Cube[i]);
            }
        }
        public void DrawLine(int x1, int y1, int x2, int y2,SpriteBatch s)
        {
            s.Draw(Game1.dot, new Vector2(x1 + offsetx, y1 + offsety), null, Color.White, (float)Math.Atan2(y2 - y1, x2 - x1), new Vector2(0,4), new Vector2((float)Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) / 8, 1), SpriteEffects.None, 1);
        }
        public void DrawLine(Vector3 a, Vector3 b, SpriteBatch s)
        {
            DrawLine((int)a.X, (int)a.Y, (int)b.X, (int)b.Y, s);
        }
        public void rotate(Vector3 v, Vector3 rot,out Vector3 temp)
        {
            temp = new Vector3(v.X, v.Y, v.Z);
            double alpha = Math.Atan2(temp.Y, temp.X);
            float l = (float)Math.Sqrt(temp.X * temp.X + temp.Y * temp.Y);
            temp.X = l * (float)Math.Cos(alpha + rot.X);
            temp.Y = l * (float)Math.Sin(alpha + rot.X);

            alpha = Math.Atan2(temp.Z, temp.X);
            l = (float)Math.Sqrt(temp.X * temp.X + temp.Z * temp.Z);
            temp.X = l * (float)Math.Cos(alpha + rot.Y);
            temp.Z = l * (float)Math.Sin(alpha + rot.Y);

            alpha = Math.Atan2(temp.Z, temp.Y);
            l = (float)Math.Sqrt(temp.Y * temp.Y + temp.Z * temp.Z);
            temp.Y = l * (float)Math.Cos(alpha + rot.Z);
            temp.Z = l * (float)Math.Sin(alpha + rot.Z);
        }
    }
}
